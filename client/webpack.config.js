const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');


const isProduction = process.env.NODE_ENV === 'production';
const envName = isProduction ? "production" : "development";


const config = {
    devServer: {
        clientLogLevel: 'info',
        contentBase: './dist/',
        historyApiFallback: true,
        overlay: {
            errors: true,
            warnings: false,
        },
        port: 9003,
        publicPath: '/',
        stats: {
            modules: false,
            chunks: false,
        }
    },
    mode: envName,
    devtool: 'source-map',
    entry: path.join(__dirname, 'src', 'index.tsx'),
    resolve: {
        alias: {
            components: path.resolve(__dirname, "src/components/"),
            app_redux: path.resolve(__dirname, "src/redux/"),
            shared: path.resolve(__dirname, "src/shared/"),
            services: path.resolve(__dirname, "src/services/"),
            environments: path.resolve(__dirname, "src/environments/"),
            containers: path.resolve(__dirname, "src/containers")
        },
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
    },
    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                use: [
                    {
                        loader: 'ts-loader'
                    }
                ],
                exclude: /node_modules/,
            },
            {
                enforce: "pre",
                test: /\.js$/,
                loader: "source-map-loader"
            },
            {
                test: /\.css$/, use: 'css-loader'
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    !isProduction ? 'style-loader': MiniCssExtractPlugin.loader,
                    // {
                    //     loader: 'style-loader',
                    //     options: {
                    //         injectType: 'singletonStyleTag'
                    //     },
                    // },
                    'css-loader',
                    // {
                    //     loader: 'css-loader',
                    //     options: {
                    //         importLoaders: 1,
                    //         sourceMap: !isProduction,
                    //         modules: true,
                    //         camelCase: true,
                    //         localIdentName: '[name]-[local]-[hash:base64:5]',
                    //     },
                    // },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                            implementation: require('node-sass')
                        },
                    }
                ],
            }
        ],
    },
    output: {
        filename: 'bundle.js',
        path: path.join(__dirname, 'dist'),
        publicPath: '/',
    },
    plugins: [
        new HtmlWebpackPlugin({
            inject: true,
            template: './public/index.html',
        }),
        new webpack.DefinePlugin({ "process.env.NODE_ENV": JSON.stringify(envName) }),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: !isProduction ? '[name].css' : '[name].[hash].css',
            chunkFilename: !isProduction ? '[id].css' : '[id].[hash].css',
            ignoreOrder: true
          }),
        new CopyPlugin([
            { from: 'src/assets', to: path.join(__dirname, 'dist/assets')}
        ])
    ],
    watchOptions: {
        ignored: /dist/,
    },
};

module.exports = config;