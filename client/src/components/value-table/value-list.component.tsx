import React from 'react';
import { message, Card, Icon, Row, Col, Spin } from 'antd';
import "./value-list.component.scss";
import { ValueViewModel } from 'shared/models/value.model';

export const ValueListComponent: React.FC<any> = (props) => {
  const { Meta } = Card;

  const { valueData, isLoading } = props;

  const cardItems = valueData.map((item: ValueViewModel) => {
    return (
      <Card hoverable style={{ maxWidth: 260, minWidth: 200 }}
        key={item.id}
        cover={<img alt="card" src={item.imgUrl} />}
        actions={[
          <Icon type="edit" key="edit" />,
        ]}>
        <Meta title={item.id} description={item.text} />
      </Card>
    );
  });

  let columns: any = [];
  const cardItemRows: any = [];
  cardItems.forEach((card, index, arr) => {
    columns.push(<Col key={index} span={4} >{card}</Col>);
    if (columns.length == 3) {
      cardItemRows.push(<Row key={columns.length - index} gutter={16}>{columns}</Row>);
      columns = [];
    }
  });

  return (
    <Spin spinning={isLoading} tip="Loading...">
      <div className="card-list">
        {cardItemRows}
      </div>
    </Spin>
  );

}