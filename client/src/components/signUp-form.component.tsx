import React from 'react'
import { Form, Button, Checkbox, Icon, Input } from "antd";

export const SignUpForm: React.FC<any> = (props) => {

    const confirmDirty = false;

    let handleSubmit = (event) => {
        event.preventDefault();
        props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                const { signUp } = props;
                signUp({   
                        firstName: values.firstName,
                        lastName: values.lastName,
                        email: values.email,
                        password: values.password 
                    });
            }
        });
    };

    const { getFieldDecorator } = props.form;
    const formItemLayout = {
        labelCol: { span: 4 },
        wrapperCol: { span: 24 },
    };

    let comparePassword = (rule, value, callback) => {
        const { form } = props;
        if (value && value !== form.getFieldValue('password')) {
          callback('Two passwords that you enter is inconsistent!');
        } else {
          callback();
        }
      };
    
      let validateToNextPassword = (rule, value, callback) => {
        const { form } = props;
        if (value && confirmDirty) {
          form.validateFields(['confirm'], { force: true });
        }
        callback();
      };

    return (
        <div className="auth-container">

            <Form layout="horizontal" onSubmit={handleSubmit} className="auth-form">
            <Form.Item {...formItemLayout}>
                    {getFieldDecorator('firstName', {
                        rules: [{ required: true, message: 'Please input your Name!' }],
                    })(
                        <Input
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="First Name"
                        />,
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout}>
                    {getFieldDecorator('lastName', {
                        rules: [{ required: true, message: 'Please input your Surname!' }],
                    })(
                        <Input
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="Last Name"
                        />,
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout}>
                    {getFieldDecorator('email', {
                        rules: [{ required: true, message: 'Please input your E-mail!' }],
                    })(
                        <Input
                            prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="E-mail"
                        />,
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout} hasFeedback>
                    {getFieldDecorator('password', {
                        rules: [
                            { required: true, message: 'Please input your Password!' },
                            { validator: validateToNextPassword}
                        ],
                    })(
                        <Input.Password
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="Password"
                        />,
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout} hasFeedback>
                    {getFieldDecorator('confirmPassword', {
                        rules: [
                            { required: true, message: 'Please confirm your password!' },
                            {validator: comparePassword}
                        ],
                    })(
                        <Input.Password
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="Confirm Password"
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    <Button size="large" shape="round" type="primary" htmlType="submit" className="app-button">
                        Sign up
                    </Button>
                    Or <a href="/account/login">back to login</a>
                </Form.Item>
            </Form>
        </div>
    );
}