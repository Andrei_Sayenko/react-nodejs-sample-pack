import React from 'react'
import { Form, Button, Checkbox, Icon, Input } from "antd";

export const LoginForm: React.FC<any> = (props) => {

    let handleSubmit = (event) => {
        event.preventDefault();
        props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                const { login } = props;
                login({ email: values.email, password: values.password });
            }
        });
    };

    const { getFieldDecorator } = props.form;
    const formItemLayout = {
        labelCol: { span: 4 },
        wrapperCol: { span: 24 },
    };

    return (
        <div className="auth-container">

            <Form layout="horizontal" onSubmit={handleSubmit} className="auth-form">
                <Form.Item {...formItemLayout}>
                    {getFieldDecorator('email', {
                        rules: [{ required: true, message: 'Please input your E-mail!' }],
                    })(
                        <Input
                            prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="E-mail"
                        />,
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout}>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Please input your Password!' }],
                    })(
                        <Input.Password
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="Password"
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('remember', {
                        valuePropName: 'checked',
                        initialValue: true,
                    })(<Checkbox>Remember me</Checkbox>)}
                    <a className="login-form-forgot" href="/">
                        Forgot password
              </a>
                    <Button loading={props.isLoading}
                        size="large" shape="round"
                        type="primary" htmlType="submit"
                        className="app-button">
                        Let's Go
              </Button>
                    Or <a href="/account/registration">register now!</a>
                </Form.Item>
            </Form>
        </div>
    );
}