export interface Environment {
    production: boolean,
    envName: string,
    apiUrl: string,   
}