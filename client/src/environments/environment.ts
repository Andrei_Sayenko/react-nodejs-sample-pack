import { Environment } from "./environment.model";

export const environment: Environment = {
    production: false,
    envName: "development",
    apiUrl: "http://localhost:5000/api",   //fakeDataUrl: 'https://randomuser.me/api/?results=5&inc=name,gender,email,nat&noinfo';
};