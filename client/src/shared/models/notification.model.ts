import { NotificationType } from "shared/enums/notification-severity.enum";

export class HubNotificationViewModel {
    message: string = "";
    icon: string = "";
    severity: NotificationType = 0;
}
