import IUser from "./user.model";

export interface AuthResponseModel {
    token: string;
    expiresIn: number;
    user: IUser;
  }