import { RoleType } from "shared/enums";

export default interface IUser{
    id: number;
    fullName: string;
    email: string;
    role: RoleType;
}