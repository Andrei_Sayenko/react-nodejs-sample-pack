import AuthorizationService from "services/authorization.service";

export function onlyUnauthorized(props: any){
    const isLoggedOut = AuthorizationService.isLoggedOut();
    return new Promise((resolve, reject) => {
      const { history } = props;
      if(isLoggedOut) {
        resolve({ isLoggedOut })
      } else{
        history.replace("/dashboard/values");
        reject();
      }
    });
}