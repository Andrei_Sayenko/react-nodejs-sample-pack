import AuthorizationService from "services/authorization.service";
import { RoleType } from "shared/enums";

export function onlyAdminRole(props: any) {
  const role = AuthorizationService.user.role;
  return new Promise((resolve, reject) => {
    const { history } = props;
    if (role == RoleType.Admin) {
      resolve(true);
    } else {
      history.replace("/dashboard/values");
      reject();
    }
  });
}