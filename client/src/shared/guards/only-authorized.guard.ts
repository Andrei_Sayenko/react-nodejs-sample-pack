
import AuthorizationService from "services/authorization.service";

export function onlyAuthorized(props: any) {
  const isLoggedIn = AuthorizationService.isLoggedIn();
  return new Promise((resolve, reject) => {
    const { history } = props;
    if(isLoggedIn) {
      resolve({ isLoggedIn })
    } else{
      history.replace("/account/login");
      reject();
    }
  });
}