import React from "react";
import { Layout } from "antd";
import "./auth.layout.scss";

const { Content } = Layout;

export default function AuthLayout(props) {
    const { children } = props;
    return (
        <Layout className="auth-layout">
            <Content>
                {children}
            </Content>
        </Layout>
    )
}