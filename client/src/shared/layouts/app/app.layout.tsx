import React from "react";
import { Layout, Menu, Icon } from "antd";
import "./app.layout.scss";

const { Content, Sider, Header } = Layout;

interface State {
    collapsed: boolean;
}

export class AppLayout extends React.Component<any,State> {

    constructor(props){
        super(props);
        this.state = {
            collapsed: false
        };
    }

    private toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
          });
    }

    private navigateTo(pathName: string){
        const {history} = this.props;
        history.push(pathName);
    }

    render (){
        return(<Layout>
            <Sider className="sidebar-panel" trigger={null} collapsible collapsed={this.state.collapsed}>
                <div className="logo" ><span>Anuitex</span></div>
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                    <Menu.Item key="1" onClick={(cp) => this.navigateTo("/dashboard/values")}>
                        <Icon type="book" />
                        <span className="nav-text">Values</span>
                    </Menu.Item>
                    <Menu.Item key="2" onClick={(cp) => this.navigateTo("/dashboard/users")}>
                        <Icon type="user" />
                        <span className="nav-text">Users</span>
                    </Menu.Item>
                    <Menu.Item key="3">
                        <Icon type="setting" />
                        <span className="nav-text">Settings</span>
                    </Menu.Item>
                </Menu>
            </Sider>
            <Layout>
                <Header style={{ background: '#fff', padding: 0 }}>
                    <Icon
                        className="trigger"
                        type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                        onClick={this.toggle}
                    />
                </Header>
                <Content>
                    {this.props.children}
                </Content>
            </Layout>
        </Layout>);
    }

    componentDidMount(){
        
    }

}