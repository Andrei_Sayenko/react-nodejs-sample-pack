import axios from "axios"
import { getItem, removeItem } from 'services/local-storage.service';
import { environment } from 'environments/environment';

const AxiosInstance = axios.create({
  baseURL: environment.apiUrl,
  headers: {
    'Content-Type': 'application/json',
  }
})

AxiosInstance.interceptors.request.use(async (config) => {
  let token = getItem("jwt_token");
  config.headers.Authorization = `Bearer ${token}`;
  return config;
});

AxiosInstance.interceptors.response.use(async (response) => {
  return response;
}, (error) => {
  const originalRequest = error.config;
  if (!error.response) {
    return Promise.reject('Network Error');
  }
  if ((error.response.status === 401) && !originalRequest._retry) {
      removeItem('jwt_token');
    return;
  }
  // handle an error here - logger, etc..
  // call notify from service to emit message for subscribers
  return error.response
});
export default AxiosInstance;