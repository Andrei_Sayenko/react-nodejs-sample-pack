import { dynamicWrapper } from "react-router-guard";
import { onlyUnauthorized, onlyAuthorized } from "shared/guards";
import { AppLayout } from "shared/layouts/app/app.layout";
import { UsersComponent } from "containers/users/users.view";

export default [
  {
    path: "/",
    redirect: "/account/login",
    exact: true
  },
  {
    path: "/dashboard",
    component: AppLayout,//dynamicWrapper(() => import("./shared/layouts/app/app.layout")),
    canActivate: [onlyAuthorized],
    routes: [
      {
        path: "/dashboard",
        redirect: "/dashboard/values",
        exact: true
      },
      {
        path: "/dashboard/values",
        component: dynamicWrapper(() => import("./containers/home/home.view"))
      },
      {
        path: "/dashboard/users",
        component: UsersComponent,//dynamicWrapper(() => import("./containers/users/users.view")),
        // canActivate: [onlyAdminRole]
      }
    ]
  },
  {
    path: "/account",
    component: dynamicWrapper(() => import("./shared/layouts/auth/auth.layout")),
    canActivate: [onlyUnauthorized],
    routes: [
      {
        path: "/account",
        redirect: "/account/login",
        exact: true,
      },
      {
        path: "/account/login",
        component: dynamicWrapper(() => import("./containers/auth/login/login.view"))
      },
      {
        path: "/account/registration",
        component: dynamicWrapper(() => import("./containers/auth/sign-up/sign-up.view"))
      }
    ]
  }
];