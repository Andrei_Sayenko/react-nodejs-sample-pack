import { Reducer, combineReducers } from "redux";
import { authReducer } from './auth/auth.reducer';
import { AuthState } from './auth/types';
import {homeReducer} from "./home/home.reducer";
import { HomeState } from "./home/types";


export interface RootState {
  auth: AuthState,
  app: any,
  home: HomeState
}

const rootReducer: Reducer<RootState> = combineReducers<RootState>({
  app: appReducer,
  auth: authReducer,
  home: homeReducer
});

function appReducer(state: any = { isLoading: false }, action: any) {
  switch (action.type) {
    case 'DATA_IS_LOADING':
      return {
        ...state,
        isLoading: action.isLoading,
      }
    default:
      return { ...state }
  }
}

export default rootReducer; 