import { HomeType } from "./types";
import ValuesService from "services/values.service"
import { AxiosResponse } from "axios";
// import { isLoading } from "app_redux/app.action";
import { ValueViewModel } from "shared/models/value.model";


function isLoading(awaiting: boolean) {
    return {
        type: HomeType.IS_LOADING,
        awaiting
    }
}

function getValuesSuccess(valueData: ValueViewModel[]) {
    return {
        type: HomeType.GET_VALUES_SUCCESS,
        valueData
    }
}

export function getValues() {
    return (dispatch: any) => {
        dispatch(isLoading(true));
        /*MOCK DATA*/
        setTimeout(() => {
            dispatch(getValuesSuccess([
                { id: 1, text: "Value1", imgUrl: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" },
                { id: 2, text: "Value2", imgUrl: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" },
                { id: 3, text: "Value3", imgUrl: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" }
            ]));
            dispatch(isLoading(false));
        }, 1000);

        /*API REQUEST */
        // ValuesService.getValues().then((resp: AxiosResponse<ValueViewModel[]>) => {
        //     if(resp){
        //         dispatch(getValuesSuccess(resp.data));
        //     }
        //     dispatch(isLoading(false));
        // });
    }
}
