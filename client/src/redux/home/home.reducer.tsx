import { HomeState, HomeType } from "./types"


const initialState: HomeState = {
    valueData: [],
    isLoading: false
}

export function homeReducer(state: HomeState = initialState, action: any) {
    switch (action.type) {
        case HomeType.GET_VALUES_SUCCESS:
            return {
                ...state,
                valueData: action.valueData
            }
            case HomeType.IS_LOADING:
            return {
                ...state,
                isLoading: action.awaiting
            }
        default:
            return { ...state }
    }
}