export enum HomeType{
    GET_VALUES_SUCCESS = "GET_VALUES_SUCCESS",
    IS_LOADING = "IS_LOADING"
}

export interface HomeState{
    valueData: any,
    isLoading: boolean
}