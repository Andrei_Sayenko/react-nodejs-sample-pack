
export enum AuthType{
    SIGN_IN_SUCCESS = "SIGN_IN_SUCCESS",
    SIGN_UP_SUCCESS = "SIGN_UP_SUCCESS",
}

export interface AuthState{
    isLoggedIn: boolean;
    authData: any
}