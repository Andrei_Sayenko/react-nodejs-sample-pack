import { AuthType } from "./types";
import { LoginViewModel } from "shared/models/login.model";
import { isLoading } from "app_redux/app.action";
import AuthorizationService from "services/authorization.service";
import { AxiosResponse } from "axios";
import { SignUpViewModel } from "shared/models/sign-up.model";
import { AuthResponseModel } from "shared/models";
import { setItem } from "services/local-storage.service";
import { RoleType } from "shared/enums";


function loginSuccess(authData: any) {
    return {
        type: AuthType.SIGN_IN_SUCCESS,
        authData,
        // providerName
    }
}
function singUpSuccess() {
    return {
        type: AuthType.SIGN_UP_SUCCESS,
        // providerName
    }
}

export function login(viewModel: LoginViewModel/*, providerName: string*/) {
    return (dispatch: any) => {
        dispatch(isLoading(true));
        /* MOCK DATA */
        AuthorizationService.user = { id: 1, email: "local.cloudmonix@gmail.com", fullName: "Dmitry Parfenov", role: RoleType.User }
        setTimeout(() => {
            dispatch(loginSuccess({}));
            dispatch(isLoading(false));
        }, 3000);

        /* API REQUEST */
        // AuthorizationService.signIn(viewModel).then((response: AxiosResponse<AuthResponseModel>) =>{
        //     AuthorizationService.user = response.data.user;
        //     setItem("jwt_token", response.data.token);
        //     setItem("exp_in", JSON.stringify(response.data.expiresIn));
        //     AuthorizationService.user = response.data.user;
        //     dispatch(loginSuccess(response.data.user));
        //     dispatch(isLoading(false));
        // });

    }
}

export function signUp(viewModel: SignUpViewModel, /*providerName: string*/) {
    return (dispatch: any) => {
        dispatch(isLoading(true));
        AuthorizationService.signUp(viewModel).then((response: AxiosResponse<string>) => {
            if (response) {
                dispatch(singUpSuccess());
            }
            dispatch(isLoading(false));
        });


    }
}
