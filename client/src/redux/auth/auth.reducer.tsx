import { AuthState, AuthType } from "./types"


const initialState: AuthState = {
    isLoggedIn: false,
    authData: {}
}

export function authReducer(state: AuthState = initialState, action: any) {
    switch (action.type) {
        case AuthType.SIGN_IN_SUCCESS:
            return {
                ...state,
                isLoggedIn: true,
                authData: action.authData
            }
        case AuthType.SIGN_UP_SUCCESS:
            return {
                ...state
            }
        default:
            return { ...state }
    }
}
