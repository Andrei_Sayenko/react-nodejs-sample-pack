
export function isLoading(isLoading: boolean) {
    return {
        type: 'DATA_IS_LOADING',
        isLoading: isLoading
    };
}