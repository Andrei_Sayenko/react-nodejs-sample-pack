import { Store, createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer, { RootState } from "./root.reducer";
import { composeWithDevTools } from "redux-devtools-extension";

export default function configureStore(initState?: RootState): Store<RootState> {
    const composeEnhancers = composeWithDevTools({
        shouldCatchErrors: true
    });
    const enhancer = composeEnhancers(applyMiddleware(thunk));
    
    const store = createStore(rootReducer, initState!, enhancer);


    return store;
}