import React, { Component, useContext } from 'react';
import './App.scss';
import { Provider } from 'react-redux';
import configureStore from './redux/store';
import { RootState } from 'redux/root.reducer';
import { Store } from "redux";
import { RouterGuard } from "react-router-guard";
import * as routerConfig from './router-config';

const store: Store<RootState> = configureStore();


class App extends Component {
  private routes: any[];
  constructor(props) {
    super(props);
    this.state = {}
    this.routes = routerConfig.default;
  }
  render() {
    return (
      <Provider store={store}>
        <RouterGuard config={this.routes} />
      </Provider>
    )
  }

}
export default App;