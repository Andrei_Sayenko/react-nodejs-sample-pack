import React from "react";
import './sign-up.view.scss'
import { Form } from "antd";
import { SignUpForm } from "components/signUp-form.component";
import { RootState } from "app_redux/root.reducer";
import { SignUpViewModel } from "shared/models/sign-up.model";
import { connect } from 'react-redux';
import * as authAction from "app_redux/auth/auth.action";

interface Props{
    signUp: (viewModel: SignUpViewModel) => void;
}
interface State{
    
}

export class SignUpComponent extends React.PureComponent<Props, State>{
    
    constructor(props: Props){
        super(props);
        this.state = {};
    }

    render(){
        const WrapperSignUpForm = Form.create({ name: 'normal_registration' })(SignUpForm);
        return(
            <div style={{background: "ghostwhite", boxShadow: '5px 7px #eee', borderRadius: '7px', paddingTop: '20px'}}>
                <span className="header-auth">Registration</span>
                <WrapperSignUpForm {...this.props} />
            </div>
        );
    }
    componentDidMount(){

    }
}

const mapStateToProps = (state: RootState) => ({
    isLoggedIn: state.auth.isLoggedIn
});

const mapDispatchToProps = (dispatch: any) => ({
    signUp: (viewModel: SignUpViewModel) => dispatch(authAction.signUp(viewModel))
});
export default connect(mapStateToProps, mapDispatchToProps)(SignUpComponent)