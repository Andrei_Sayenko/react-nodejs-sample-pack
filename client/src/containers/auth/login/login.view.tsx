import React from "react";
import { connect } from 'react-redux';
import * as authAction from "app_redux/auth/auth.action";
import { LoginViewModel } from "shared/models";
import { Form } from "antd";
import { LoginForm } from "components/login-form.component"
import { RootState } from "app_redux/root.reducer";
import './login.view.scss'
import { Redirect } from "react-router";

interface Props {
    login: (viewModel: LoginViewModel/*, providerName: string*/) => void;
    isLoggedIn: boolean;
    isLoading: boolean;
}

interface State {
}

export class LoginComponent extends React.PureComponent<Props, State>{

    constructor(props: Props) {
        super(props);
        // State should be initialized in ctor
        this.state = {
        };
    }
    
    render() {
        if(this.props.isLoggedIn){
            const { history }: any = this.props;
            history.push('/');
        }
        const WrapperLoginForm = Form.create({ name: 'normal_login' })(LoginForm);
        return (
            <div style={{background: "ghostwhite", boxShadow: '5px 7px #eee', borderRadius: '7px', paddingTop: '20px'}}>
                <span className="header-auth">Login</span>
                <span className="sub-title">Sign in to your account.</span>
                <WrapperLoginForm {...this.props} />
            </div>
        );
    }

    componentDidMount() {
    }

}

const mapStateToProps = (state: RootState) => ({
    isLoggedIn: state.auth.isLoggedIn,
    isLoading: state.app.isLoading
});

const mapDispatchToProps = (dispatch: any) => ({
    login: (viewModel: LoginViewModel) => dispatch(authAction.login(viewModel))
});
export default connect(mapStateToProps, mapDispatchToProps)(LoginComponent)