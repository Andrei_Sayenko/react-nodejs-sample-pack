import React from "react";
import { ValueListComponent } from "components/value-table/value-list.component";
import { connect } from "react-redux";
import { RootState } from "app_redux/root.reducer";
import * as homeActions from "app_redux/home/home.action";
import { ValueViewModel } from "shared/models/value.model";

interface Props {
    getValues: () => ValueViewModel[];
    isLoading: boolean;
    valueData: ValueViewModel[];
}

interface State {

}

export class HomeComponent extends React.Component<Props, State>{

    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    render() {
        const { valueData, isLoading } = this.props;
        return (
            <div>
                <ValueListComponent valueData={...valueData} isLoading={isLoading} />
            </div>
        );
    }

    componentDidMount() {
        const { getValues } = this.props;
        getValues();
    }
}


const mapStateToProps = (state: RootState) => ({
    isLoading: state.home.isLoading,
    valueData: state.home.valueData
});

const mapDispatchToProps = (dispatch: any) => ({
    getValues: () => dispatch(homeActions.getValues()),
});
export default connect(mapStateToProps, mapDispatchToProps)(HomeComponent);