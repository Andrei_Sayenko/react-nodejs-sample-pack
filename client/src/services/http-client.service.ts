import { AxiosPromise } from "axios";
import AxiosInstance from "shared/interceptors/http.interceptor";


export function post(url: string, data: object): AxiosPromise {
    return AxiosInstance.post(url, data);
}

export function get(url: string): AxiosPromise {
    return AxiosInstance.get(url);
}

export function put(url: string, data: object): AxiosPromise {
    return AxiosInstance.put(url, data);
}