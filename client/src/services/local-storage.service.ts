
export const getItem = (key: string) => {
    if (typeof window === "undefined" || !localStorage) {
        return null;
    }
    return localStorage.getItem(key);
}

export const removeItem = (key: string) => {
    if (typeof window === "undefined" || !localStorage) {
        return;
    }
    localStorage.removeItem(key);
}

export const setItem = (key: string, value: string) => {
    if (typeof window === "undefined" || !localStorage) {
        return;
    }
    localStorage.setItem(key, value);
}