import { AxiosPromise } from "axios";
import * as httpClient from "./http-client.service";
import { ValueViewModel } from "shared/models/value.model";

class ValuesService{

    public getValues(): AxiosPromise<ValueViewModel> {
        return httpClient.get("/value/get");
    }
}

export default new ValuesService();