import { HubNotificationViewModel } from "shared/models";
import { BehaviorSubject, ReplaySubject } from "rxjs";
import { NotificationType } from "shared/enums/notification-severity.enum";


export class NotificationService {

    public notificationReceived: ReplaySubject<HubNotificationViewModel> = new ReplaySubject();

    public notify(responseObj: any) {
        let notificationModel = new HubNotificationViewModel();
        notificationModel.message = responseObj.message;
        notificationModel.severity = responseObj.type;
        notificationModel.icon = responseObj.icon;

        this.notificationReceived.next(notificationModel);
    }

}