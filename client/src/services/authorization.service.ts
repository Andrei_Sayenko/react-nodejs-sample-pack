import { LoginViewModel } from "shared/models/login.model";
import * as httpClient from "./http-client.service";
import { AxiosPromise } from "axios";
import { BehaviorSubject, Observable } from "rxjs";
import { getItem, setItem } from "./local-storage.service";
import { AuthResponseModel } from "shared/models";
import IUser from "shared/models/user.model";

const constants = {
    currentUser: "currentUser",
};

class AuthorizationService {


    private currentUserSubject: BehaviorSubject<IUser>;
    public currentUser: Observable<IUser>;

    constructor() {
        const authData = this.getUserFromStorage();
        this.currentUserSubject = new BehaviorSubject<any>(authData);
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public signIn(viewModel: LoginViewModel): AxiosPromise<AuthResponseModel> {
        return httpClient.post("/auth/login", viewModel);
    }
    public signUp(viewModel: LoginViewModel): AxiosPromise<string> {
        return httpClient.post("/auth/register", viewModel);
    }

    private getUserFromStorage(): IUser {
        const userData = getItem(constants.currentUser);
        return !userData ? null : JSON.parse(userData);
    }

    public get user(): IUser{
        return this.currentUserSubject.value;
    }

    public set user(authData: IUser) {
        setItem(constants.currentUser, JSON.stringify(authData));
        this.currentUserSubject.next(authData);
    }

    public isLoggedIn(): boolean {
        const authData = getItem(constants.currentUser);
        return !!authData;
      }
    
      public isLoggedOut(): boolean {
        const authData = getItem(constants.currentUser);
        return !authData;
      }
}

export default new AuthorizationService();