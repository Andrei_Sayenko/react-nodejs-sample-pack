export class UserEntity {
  id: number;
  email: string;
  fullName: string;
  password: string;
}
