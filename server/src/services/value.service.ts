import { ValueModel, ValueCreateModel } from "../models";
import { ValueRepository } from "../repositories";
import { ValueEntity } from "../entities";
import { inject, injectable } from "inversify";

@injectable()
export class ValueService {
  constructor(
    @inject(ValueRepository) private valueRepository: ValueRepository
  ) {}

  async add(value: ValueCreateModel) {
    const valueEntity: ValueEntity = {
      text: value.text,
      id: null
    };
    this.valueRepository.add(valueEntity);
    const valueModel: ValueModel = {
      id: valueEntity.id,
      text: valueEntity.text
    };
    return valueModel;
  }

  async get() {
    const values = await this.valueRepository.get();
    const valueModels = values.map<ValueModel>(value => {
      return {
        id: value.id,
        text: value.text
      };
    });
    return valueModels;
  }
}
