import { AuthRegisterModel, AuthUserModel } from "../models";
import { UserRepository } from "../repositories";
import { inject, injectable } from "inversify";
import { ApplicationError } from "common";
import { HashEncrypter } from "common/hash-encrypter";
@injectable()
export class AuthService {
  constructor(
    @inject(UserRepository) private _userRepository: UserRepository,
    @inject(HashEncrypter) private _hashEncrypter: HashEncrypter
  ) {}

  async register(registerModel: AuthRegisterModel): Promise<AuthUserModel> {
    const hashedPassword: string = this._hashEncrypter.getHash(
      registerModel.password
    );
    const existedUser = await this._userRepository.findOne(
      registerModel.email,
      hashedPassword
    );
    if (existedUser) {
      throw new ApplicationError("User already exist!");
    }
    const userEntity = await this._userRepository.add({
      email: registerModel.email,
      fullName: registerModel.fullName,
      password: hashedPassword,
      id: null
    });
    return userEntity;
  }

  async get(login: string, password: string): Promise<AuthUserModel> {
    const hashedPassword = this._hashEncrypter.getHash(password);
    const value = await this._userRepository.findOne(login, hashedPassword);
    return {
      id: value.id,
      email: value.email,
      fullName: value.fullName,
      password: value.password
    };
  }
}
