import { AuthMiddleware } from "middlewares/auth.middleware";
import { ValueService } from "services";
import { ValueCreateModel } from "models";
import { diContainer } from "di-container";
import { injectable, inject } from "inversify";
import { RequestPost, Controller, RouteHandler } from "common";

@injectable()
export class ValueController implements Controller {
  @inject(ValueService) private _valueService: ValueService;

  constructor() {
    this.add = this.add.bind(this);
    this.get = this.get.bind(this);
  }

  async add(req: RequestPost<ValueCreateModel>) {
    return await this._valueService.add({
      text: req.body.text
    });
  }

  async get() {
    return await this._valueService.get();
  }

  routes(): RouteHandler[] {
    const handlers: RouteHandler[] = [];
    const prefix = "value";
    handlers.push({
      route: `/${prefix}/get`,
      handlers: [AuthMiddleware, this.get],
      type: "GET"
    });
    handlers.push({
      route: `/${prefix}/create`,
      handlers: [AuthMiddleware, <any>this.add],
      type: "POST"
    });
    return handlers;
  }
}
