export interface AuthRegisterModel {
  email: string;
  password: string;
  fullName: string;
}
