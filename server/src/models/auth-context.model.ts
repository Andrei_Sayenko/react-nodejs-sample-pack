export interface AuthContextModel {
  id: number;
  fullName: string;
  email: string;
}
