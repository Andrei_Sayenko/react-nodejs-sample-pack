export interface ValueModel {
  id: number;
  text: string;
}
