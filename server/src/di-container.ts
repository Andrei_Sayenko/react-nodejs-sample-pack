import { Container } from "inversify";
import { ValueService } from "./services";
import { AuthService } from "./services/auth.service";
import { UserRepository, ValueRepository } from "./repositories";
import { ValueController, AuthController } from "./controllers";
import { JwtHelper, Controller, HashEncrypter } from "common";

export const diContainer = new Container();

diContainer.bind<JwtHelper>(JwtHelper).toSelf();
diContainer.bind<HashEncrypter>(HashEncrypter).toSelf();
diContainer.bind<UserRepository>(UserRepository).toSelf();
diContainer.bind<ValueRepository>(ValueRepository).toSelf();

diContainer.bind<ValueService>(ValueService).toSelf();
diContainer.bind<AuthService>(AuthService).toSelf();

diContainer.bind<Controller>("Controller").to(ValueController);
diContainer.bind<Controller>("Controller").to(AuthController);
