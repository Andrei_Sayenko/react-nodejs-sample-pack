import { ValueEntity } from "../entities/value.entity";
import { injectable } from "inversify";
import { Model } from "sequelize";

export class ValueModel extends Model implements ValueEntity {
  id: number;
  text: string;
}

@injectable()
export class ValueRepository {
  constructor() {
  }
  async add(valueEntity: ValueEntity): Promise<ValueEntity> {
    let newValue = await ValueModel.create(valueEntity);
    valueEntity = newValue;
    return valueEntity;
  }

  async get(): Promise<ValueEntity[]> {
    var result: any = await ValueModel.findAll();
    return result;
  }
}
